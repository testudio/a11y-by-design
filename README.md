# Accessibility-by-design

This document contains a list of resources to accompany presentation [Accessibility in WYSIWYG editors](https://docs.google.com/presentation/d/1NUCLepwa8txKw7LPcnPhBkPkWowSVcN8uVHvcAW4rkM/edit) from [DrupalSouth Sydney 2024](https://drupalsouth.org/events/drupalsouth-sydney-2024/schedule/3013).

## In Discovery stage
- Define level of required compliance
- Consider diversity of users, including people from different cultural backgrounds and people with disability
- Define types of environments, different browsers and desktop and mobile devices, slow connections, limited data bandwidth
- Find your accessibility champion - someone who’s familiar with accessibility requirements and best practices for development and content
- Research tools and platforms for testing

## In Design stage
While developing requirements and prototypes:
- Educate your designers, UI/UX specialists about WCAG levels
  - 1.3 Adaptable 
  - 1.4.1 Use of Color 
  - 1.4.3 Contrast (Minimum) 
  - 1.4.13 Content on Hover or Focus 
  - 2.4.7 Focus Visible
- Perform accessibility testing on design prototypes. It's faster to solve issues in prototyping phase by updating colours/contrasts/layouts then fixing it after implementation in development stage.

## In Development stage
- Educate your developers and testers about WCAG levels, developers best practices and testing tools
- Add accessibility requirements to **the definition of done**  of the tasks
- Perform accessibility testing on various devices or platforms and selection of testing tools
- Develop accessibility policy which includes:
  - Level of accessibility compliance
  - Commitment to ongoing testing for accessibility and improvements
  - Contact details for any inquiries concerning accessibility
  - Example: https://www.dta.gov.au/accessibility 
- Investigate which Drupal modules might be suitable for your web application to enhance accessibility:
  - Automatic Alternative Text (https://www.drupal.org/project/auto_alter)
  - Block ARIA Landmark Roles (https://www.drupal.org/project/block_aria_landmark_roles)
  - Keyboard shortcuts (https://www.drupal.org/project/keyboard_shortcuts)
- Modules to test for accessibility (maybe should not be used in production):
  - Editoria11y Accessibility Checker (https://www.drupal.org/project/editoria11y)

## At go live:
- Prepare evidence that your web application meets WCAG level
- Perform ongoing testing for accessibility so that your users can continue to access the service
- Publish accessibility policy

## Drupal Accessibility
- Strong focus and commitment to comply with WCAG 2.1 AA
  - https://www.drupal.org/docs/getting-started/accessibility
- A11y Office Hours 
  - Every two months meet the team, get your questions answered and your projects reviewed
  - https://www.drupal.org/docs/getting-started/accessibility/a11y-office-hours
- Coding standards and best practices 
  - https://www.drupal.org/docs/develop/standards/accessibility-coding-standards
- Testing guide 
  - https://www.drupal.org/docs/getting-started/accessibility/how-to-do-an-accessibility-review
- List of issues
  - https://www.drupal.org/project/drupal/issues/3335980

## Drupal 10 modules for CKEditor 5 to enhance editing experience and accessibility
- CKEditor Abbreviation
  - Adds semantic tag for abbreviations, eg <abbr>
  - https://www.drupal.org/project/ckeditor_abbreviation
- Editor Advanced Link 
  - Adds extra attributes to a link 
  - https://www.drupal.org/project/editor_advanced_link
- Find/Replace
  - Adds find/replace feature
  - https://www.drupal.org/project/ckeditor5_findandreplace
- CKEditor Wordcount
  - CKEditor5 porting is WIP
  - https://www.drupal.org/project/ckwordcount
- Content accessibility checker is CKEditor 4 only
  - CKEditor 5 issue: https://github.com/ckeditor/ckeditor5/issues/1941 

## Drupal and CKEditor 5 accessibility testing

### Perceivable: 1.4.1 Use of Color
**Drupal core issue**
- Add outline to buttons with active state (fixed, not yet released)
  - https://www.drupal.org/project/drupal/issues/3306209

### Operable: 2.1 Keyboard Accessible
CKEditor 5 Keyboard navigation
- https://ckeditor.com/docs/ckeditor5/latest/features/keyboard-support.html

Magic combination to access toolbar (need to search for it in documentation): **Alt F10 (Windows) or ⌥ F10 (Mac)**

**CKEditor5 issues**
- Shortcut for Ubuntu (Linux) 
  - https://github.com/ckeditor/ckeditor5/issues/5807 
- Add help text to make toolbar/help keyboard shortcut more discoverable
  - https://github.com/ckeditor/ckeditor5/issues/16048 

**Drupal core issues**
- Update CKEditor 5 to 41.2.0 (fixed, not yet released)
  - https://www.drupal.org/project/drupal/issues/3424644
- [PP-1] Provide new AccessibilityHelp toolbar item in CKEditor 5 >=41.2
  - https://www.drupal.org/project/drupal/issues/3427039 

### Robust: 4.1.2 Name, Role, Value

During voice over test text field label has aria-label attribute which reads out: `“Editor editing area: main”`

**CKEditor5 issue**
- Make Editor title configurable again to fix accessibility issues 
  - https://github.com/ckeditor/ckeditor5/issues/15208

**Drupal core issue**
- CKEditor 5 text area doesn't have appropriate aria-label for screen reader
  - https://www.drupal.org/project/drupal/issues/3426798 


## CKEditor 4 (deprecated)
CKEditor 4 Accessibility Guide: https://ckeditor.com/docs/ckeditor4/latest/guide/dev_a11y.html 

CKEditor 4 WCAG 2.0 checklist: https://ckeditor.com/docs/ckeditor4/latest/guide/dev_wcag.html

## CKEditor 5
CKEditor 5 WCAG 2.0 checklist: https://ckeditor.com/docs/ckeditor5/latest/features/accessibility.html#accessibility-conformance-report-vpat

